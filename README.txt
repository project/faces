
Extendable Object Faces
------------------------
by Wolfgang Ziegler, nuppla@zites.net

This is an API module, only install if you want to run the tests.
Modules using the API should just ship with the include file.

Further developer docs can be found in the handbook on drupal.org:
  * http://drupal.org/node/666888


How to use the faces API without introducing a dependency?
-----------------------------------------------------------
 * Add the most recent "faces.inc" include file to your module, but don't list
   it in your module's info file.

 * Add the following code at the top of your module. Replace MODULE with your
   module's name.

CODE:
--------------------------------------------------------------------------------
 
 spl_autoload_register('MODULE_autoload');

/**
 * Autoload API includes. Note that the code registry autoload is used only
 * by the providing API module.
 */
function MODULE_autoload($class) {
  if (stripos($class, 'faces') === 0) {
    module_load_include('inc', 'MODULE', 'faces');
  }
}

--------------------------------------------------------------------------------

 That way the include is autoloaded once a class of it is used.
 Once the faces module is installed and enabled, the version of the include
 file shipping with this module is used as it doesn't rely on autoloading.
