<?php

/**
 * @file Include file for testing faces file inclusion.
 */

/**
 * Function callback used for extending.
 */
function faces_test_extension_function2($prefix, $object) {
  return $prefix . $object->name;
}
